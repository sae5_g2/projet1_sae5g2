import { useState } from 'react';

const Global = () => {

  const [edition, setEdition] = useState(false);

  const [idTheme, setIdTheme] = useState(null);

  const [idRoutine, setIdRoutine] = useState(null);

  const [idStep, setIdStep] = useState(null);

  const basculerMode = () => {
    setEdition(!edition)
  }


  return {
    edition,
    basculerMode,
    setIdTheme,
    setIdRoutine,
    setIdStep
  };


};

export default Global;
