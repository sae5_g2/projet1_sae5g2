import { React, useState } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, Modal } from 'react-native';
import Global from "../Globals/GlobalsVariables";

const Panel = ({ txt, imageSource, onPress }) => {
  const [modalVisible, setModalVisible] = useState(false);

  const { edition } = Global();

  const handlePress = () => {
    setModalVisible(true);
  };

  const handleEdit = () => {
    setModalVisible(false);
  };

  const handleDelete = () => {
    setModalVisible(false);
  };

  const handleCancel = () => {
    setModalVisible(false);
  };

  return (
    <View>
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <Image source={imageSource} style={styles.image} />
        <Text style={styles.text}>{txt}</Text>
      </TouchableOpacity>

      {edition && (
        <TouchableOpacity onPress={handlePress} style={styles.button}>
          <Text>...</Text>
        </TouchableOpacity>
      )}

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}>

        <View style={styles.modal}>
          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={handleEdit} style={styles.menuButton}>
              <Text>Edit</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleDelete} style={styles.menuButton}>
              <Text>Delete</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleCancel} style={styles.menuButton}>
              <Text>Cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: 16,
  },
  image: {
    width: 150,
    height: 150,
    marginBottom: 10,
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
  },
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: '100%',
    padding: 16,
  },
  button: {
    position: 'absolute',
    top: 10,
    right: 10,
    padding: 10,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 5,
  },
  menuButton: {
    padding: 10,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 5,
    right: 250,
    top: -75,
  },
});



export default Panel;