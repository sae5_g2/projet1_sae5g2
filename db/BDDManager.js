import React, { useEffect, useState } from 'react';
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('data.db');

const BDDManager = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [themes, setThemes] = useState();
  const [routines, setRoutines] = useState();
  const [steps, setSteps] = useState();

  const getThemes = () => {
    db.transaction(tx => {
      tx.executeSql("SELECT * FROM THEME;", null, (txObj, resultSet) => {
        const myThemes = resultSet.rows._array;
        //console.log('Themes depuis getThemes de bddmanager :', myThemes);
        setThemes(myThemes);
      }, (txObj, error) => console.log('ERROR : ', error));
    });
  };

  const getRoutinesByTheme = ({ idTheme }) => {
    db.transaction(tx => {
      tx.executeSql("SELECT * FROM ROUTINE WHERE id_theme=?;", [idTheme], (txObj, resultSet) => {
        const myRoutines = resultSet.rows._array;
        console.log('Routines depuis getRoutines de bddmanager :', myRoutines);
        setRoutines(myRoutines);
      }, (txObj, error) => console.log('ERROR : ', error));
    });
  };

  const addOneTheme = ({ text, image, rangList }) => {
    console.log(text, image, rangList);
    db.transaction(tx => {
      tx.executeSql(
        'INSERT INTO THEME(path_image, nom, rang_liste, couleur, id_utilisateur) VALUES (?, ?, ?, "#FF0000", 1)',
        [image, text, rangList],
      );
    }, (txObj, error) => console.log('INSERT THEME ERROR : ', error));
    getThemes();
  };

  const addOneRoutine = ({ text, image, rangList, idTheme }) => {
    console.log(text, image, rangList, idTheme);
    db.transaction(tx => {
      tx.executeSql(
        'INSERT INTO ROUTINE(nom, path_image, rang_liste, id_theme, couleur) values (?, ?, ?, ?, "#123123")',
        [text, image, rangList, idTheme],
      );
    }, (txObj, error) => console.log('INSERT ROUTINE ERROR : ', error));
    getRoutinesByTheme({ idTheme: idTheme });
  };

  useEffect(() => {
    /*db.transaction(tx => {
      tx.executeSql('DROP TABLE IF EXISTS ETAPE_HISTORY;');
      tx.executeSql('DROP TABLE IF EXISTS ROUTINE_HISTORY;');
      tx.executeSql('DROP TABLE IF EXISTS ETAPE;');
      tx.executeSql('DROP TABLE IF EXISTS ROUTINE;');
      tx.executeSql('DROP TABLE IF EXISTS THEME;');
      tx.executeSql('DROP TABLE IF EXISTS UTILISATEUR;');
    });
    db.transaction(tx => {
      tx.executeSql('CREATE TABLE UTILISATEUR (id_utilisateur INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT NOT NULL UNIQUE, path_image INTEGER NOT NULL, couleur TEXT NOT NULL, rang_liste INTEGER NOT NULL UNIQUE);');
      tx.executeSql('CREATE TABLE THEME (id_theme INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT NOT NULL, path_image INTEGER NOT NULL, couleur TEXT NOT NULL, rang_liste INTEGER NOT NULL, id_utilisateur INTEGER NOT NULL, UNIQUE(nom, id_utilisateur), UNIQUE(rang_liste, id_utilisateur), FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur));');
      tx.executeSql('CREATE TABLE ROUTINE (id_routine INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT NOT NULL, path_image INTEGER NOT NULL, couleur TEXT NOT NULL, rang_liste INTEGER NOT NULL, id_theme INTEGER NOT NULL, UNIQUE(nom, id_theme), UNIQUE(rang_liste, id_theme), FOREIGN KEY(id_theme) REFERENCES THEME(id_theme));');
      tx.executeSql('CREATE TABLE ETAPE ( id_etape INTEGER PRIMARY KEY AUTOINCREMENT, consigne TEXT NOT NULL, path_image INTEGER NOT NULL, rang_liste INTEGER NOT NULL, audio_path TEXT, est_lue BOOLEAN NOT NULL, id_routine INTEGER NOT NULL, UNIQUE(rang_liste, id_routine), FOREIGN KEY(id_routine) REFERENCES ROUTINE(id_routine));');
      tx.executeSql('CREATE TABLE ETAPE_HISTORY (routine_history INTEGER, id_etape INTEGER, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, aide BOOLEAN NOT NULL, retour BOOLEAN NOT NULL, PRIMARY KEY(routine_history, id_etape), FOREIGN KEY(routine_history) REFERENCES ROUTINE_HISTORY(routine_history), FOREIGN KEY(id_etape) REFERENCES ETAPE(id_etape));');
      tx.executeSql('CREATE TABLE ROUTINE_HISTORY (routine_history INTEGER PRIMARY KEY AUTOINCREMENT, id_routine INTEGER NOT NULL, FOREIGN KEY(id_routine) REFERENCES ROUTINE(id_routine));');
    });
    db.transaction(tx => {
      tx.executeSql('INSERT INTO Utilisateur(nom, path_image, couleur, rang_liste) values ("Paula", "https://api.arasaac.org/v1/pictograms/2590?download=false", "#7CFC00", 1)');
    });
    db.transaction(tx => {
      tx.executeSql('INSERT INTO THEME(path_image, nom, couleur, id_utilisateur, rang_liste) values ("https://api.arasaac.org/v1/pictograms/4610?download=false", "alimentation", "#0000FF", 1, 1)');
      tx.executeSql('INSERT INTO THEME(path_image, nom, couleur, id_utilisateur, rang_liste) values ("https://api.arasaac.org/v1/pictograms/28507?download=false", "hygiène",  "#00FF00", 1, 2)');
      tx.executeSql('INSERT INTO THEME(path_image, nom, couleur, id_utilisateur, rang_liste) values ("https://api.arasaac.org/v1/pictograms/37534?download=false", "école",  "#FF0000", 1, 3)');
    });
    db.transaction(tx => {
      tx.executeSql('INSERT INTO ROUTINE(nom, path_image, couleur, rang_liste, id_theme) values ("se laver les mains", "https://api.arasaac.org/v1/pictograms/2443?download=false", "#123123", 1, 2)');
      tx.executeSql('INSERT INTO ROUTINE(nom, path_image, couleur, rang_liste, id_theme) values ("se brosser les dents", "https://api.arasaac.org/v1/pictograms/2694?download=false", "#123123", 2, 2)');
      tx.executeSql('INSERT INTO ROUTINE(nom, path_image, couleur, rang_liste, id_theme) values ("faire son cartable", "https://api.arasaac.org/v1/pictograms/2475?download=false", "#123123", 1, 3)');
      tx.executeSql('INSERT INTO ROUTINE(nom, path_image, couleur, rang_liste, id_theme) values ("faire ses devoirs", "https://api.arasaac.org/v1/pictograms/32556?download=false", "#123123", 2, 3)');
    });
    db.transaction(tx => {
      tx.executeSql('INSERT INTO ETAPE(consigne, path_image, rang_liste, est_lue, id_routine) values ("prends ton sac", "https://api.arasaac.org/v1/pictograms/2475?download=false", 1, 1, 3)');
      tx.executeSql('INSERT INTO ETAPE(consigne, path_image, rang_liste, est_lue, id_routine) values ("mets tes cahiers dans ton sac", "https://api.arasaac.org/v1/pictograms/2359?download=false", 2, 1, 3)');
      tx.executeSql('INSERT INTO ETAPE(consigne, path_image, rang_liste, est_lue, id_routine) values ("ferme ton sac", "https://api.arasaac.org/v1/pictograms/38700?download=false", 3, 1, 3)');
    });*/



    db.transaction(tx => {
      //tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name NOT LIKE 'sqlite_%';", null, (txObj, resultSet) => { const tables = resultSet.rows._array.map(table => table.name); console.log('Tables:', tables); }, (txObj, error) => console.log('ERROR : ', error));
      //tx.executeSql("SELECT * FROM UTILISATEUR;", null, (txObj, resultSet) => { const utilisateurs = resultSet.rows._array; console.log('Utilisateurs:', utilisateurs); }, (txObj, error) => console.log('ERROR : ', error));
      //tx.executeSql("SELECT * FROM THEME;", null, (txObj, resultSet) => { const myThemes = resultSet.rows._array; console.log('Themes:', myThemes); setThemes(myThemes) }, (txObj, error) => console.log('ERROR : ', error));
      //tx.executeSql("SELECT * FROM ROUTINE;", null, (txObj, resultSet) => { const myRoutines = resultSet.rows._array; console.log('Routines:', myRoutines); }, (txObj, error) => console.log('ERROR : ', error));
      tx.executeSql("SELECT * FROM ETAPE;", null, (txObj, resultSet) => { const myEtapes = resultSet.rows._array; console.log('Etapes:', myEtapes); }, (txObj, error) => console.log('ERROR : ', error));
    });
    setIsLoading(false);
  }, [db]);

  return {
    addOneRoutine, addOneTheme, getRoutinesByTheme, getThemes, setThemes, // Expose the getThemes method
    routines, themes,    // You can expose other state variables or methods as needed
  };
};

export default BDDManager;
