import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ThemesScreen from './Pages/Themes';
import RoutinesScreen from './Pages/Routines';
import StepsScreen from './Pages/Steps';

const App = () => {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Themes"
          component={() => <ThemesScreen />}
        />
        <Stack.Screen
          name="Steps"
          component={() => <StepsScreen />}
        />
        <Stack.Screen
          name="Routines"
          component={() => <RoutinesScreen />}
        />
        <Stack.Screen
          name="Update"
          component={() => <RoutinesScreen />}
        />
        <Stack.Screen
          name="Panel"
          component={() => <Panel />}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;