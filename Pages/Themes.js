import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, ScrollView, View } from 'react-native';
import Panel from "../Components/Panel";
import AddB from "../Components/AddButton";
import CreateTheme from './CreateTheme';
import BDDManager from "../db/BDDManager";
import Global from "../Globals/GlobalsVariables";


const ThemesScreen = () => {
  const { addOneTheme, getThemes, themes } = BDDManager();
  useEffect(() => {
    getThemes();
  }, []);

  const { basculerMode, edition } = Global();
  const navigation = useNavigation();

  const [isPopupVisible, setPopupVisible] = useState(false);

  const handleAddPanel = ({ text, image }) => {
    var rangList = themes.length
    rangList += 1;
    addOneTheme({ text, image, rangList })
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <TouchableOpacity onLongPress={basculerMode} style={styles.boutonMode}>
        <Text style={styles.boutonModeText}>{edition ? 'Mode Normal' : 'Mode Édition'}</Text>
      </TouchableOpacity>

      <View style={styles.panelContainer}>
        {themes && themes.map(theme => (
          <Panel
            key={theme.id}
            txt={<Text style={styles.panelText}>{theme.nom}</Text>}
            imageSource={{ uri: theme.path_image }}
            onPress={() => navigation.navigate('Routines', { theme: theme.id_theme })}
            style={styles.panel} // Ajout du style du panel
          ></Panel>
        ))}
      </View>

      {edition && (
        <AddB txt={<Text style={styles.ajoutText}>{'+'}</Text>} onPress={() => setPopupVisible(true)}></AddB>
      )}
      <CreateTheme visible={isPopupVisible} onClose={() => setPopupVisible(false)} handleAddPanel={handleAddPanel} />
    </ScrollView>
  );

}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'yellow',
    alignItems: 'flex-start',
    paddingTop: 60,
    paddingLeft: 20,
  },
  boutonMode: {
    padding: 30,
    backgroundColor: 'green',
    marginBottom: 20,
  },
  boutonModeText: {
    color: 'white',
    fontSize: 20,
  },
  panelContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  panel: {
    width: 100,
    height: 100,
    margin: 5,
  },
});

export default ThemesScreen;
