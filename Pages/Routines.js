import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Button } from 'react-native';
import Panel from "../Components/Panel.js";
import AddB from '../Components/AddButton.js';
import { useRoute, useNavigation } from '@react-navigation/native';
import CreateRoutine from './CreateRoutine';
import Global from "../Globals/GlobalsVariables";
import BDDManager from "../db/BDDManager";

const RoutinesScreen = () => {

  const navigation = useNavigation();
  const route = useRoute();
  const { theme } = route.params;
  const [isPopupVisible, setPopupVisible] = useState(false);

  const { addOneRoutine, getRoutinesByTheme, routines } = BDDManager();
  useEffect(() => {
    getRoutinesByTheme({ idTheme: theme });
    //console.log('Routines depuis Routines.js :', routines);
  }, []);

  const { basculerMode, edition } = Global();

  const handleAddPanel = ({ text, image }) => {
    var rangList = routines.length
    rangList += 1;
    addOneRoutine({ text, image, rangList, idTheme: theme })
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onLongPress={basculerMode} style={styles.boutonMode}>
        <Text style={styles.boutonModeText}>{edition ? 'Mode Normal' : 'Mode Édition'}</Text>
      </TouchableOpacity>

      <View style={styles.panelContainer}>
        {routines && routines.map(routine => (
          <Panel
            key={routine.id_routine}
            txt={<Text style={styles.panelText}>{routine.nom}</Text>}
            imageSource={{ uri: routine.path_image }}
            onPress={() => navigation.navigate('Steps', { routine: routine.id_routine })}
            style={styles.panel}
          ></Panel>
        ))}
      </View>

      <View style={styles.buttonRetour} ><Button
        style={styles.buttonRetour}
        title="retour"
        onPress={() =>
          navigation.navigate('Themes')
        }
      /></View>

      {edition && (
        <AddB txt={<Text style={styles.ajoutText}>{'+'}</Text>} onPress={() => setPopupVisible(true)}></AddB>
      )}
      <CreateRoutine visible={isPopupVisible} onClose={() => setPopupVisible(false)} handleAddPanel={handleAddPanel} />
    </View>
  );
}



const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'yellow',
    alignItems: 'flex-start',
    paddingTop: 60,
    paddingLeft: 20,
  },
  boutonMode: {
    padding: 30,
    backgroundColor: 'green',
    marginBottom: 20,
  },
  buttonRetour: {
    position: 'absolute',
    top: 10,
    right: 10,
    padding: 10,
  },
  boutonModeText: {
    color: 'white',
    fontSize: 20,
  },
});
export default RoutinesScreen;