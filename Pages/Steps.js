import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import Panel from "../Components/Panel";
import Global from "../Globals/GlobalsVariables";

const StepsScreen = ({ edition, toggleEdition }) => {
  const navigation = useNavigation();
  const route = useRoute();

  const { theme } = route.params;
  const { page } = route.params;

  let img;
  let txt;
  let content;
  let x = 1;

  // Cette partie sera remplacée par une requête SQL
  if (theme === 'alimentation') {
    content = 'alimentation'
  } else if (theme === 'hygiène') {
    content = 'hygiène'
  }

  if (page === 2) {
    img = { uri: 'https://api.arasaac.org/v1/pictograms/38555?download=false' };
    txt = "page2";
  } else {
    img = { uri: 'https://api.arasaac.org/v1/pictograms/37364?download=false' };
    txt = "autrePage";
  }

  return (
    <View style={styles.container}>
      <Panel txt={<Text>{txt}</Text>} imageSource={img}></Panel>

      <Button
        style={styles.arrow}
        title="➞"
        onPress={() => {
          x += 1
          navigation.navigate('Step', { page: x })
        }}
      />

      <Button
        style={styles.arrow}
        title="←"
        onPress={() =>
          navigation.navigate('Routines', { theme: content })
        }
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'yellow',
    alignItems: 'flex-start',
    paddingTop: 60,
    paddingLeft: 20,
  },
  arrow: {
    padding: 30,
    backgroundColor: 'green'
  }
});

export default StepsScreen;