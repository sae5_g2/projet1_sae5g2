import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Modal, TextInput, Button, Image } from 'react-native';
import * as ImagePicker from 'expo-image-picker';

const CreateRoutine = ({ visible, onClose, handleAddPanel }) => {
  const [text, setText] = useState('');
  const [image, setImage] = useState(null);

  const pickImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  const handleAdd = () => {
    // Vérifiez si le texte et l'image sont définis avant d'ajouter
    if (text && image) {
      handleAddPanel({ text, image });
      onClose();
    }
  };

  return (
    <Modal visible={visible} animationType="slide" transparent>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View style={{ backgroundColor: 'white', padding: 20, borderRadius: 10, width: 300 }}>
          <Text>Entrez le texte:</Text>
          <TextInput
            style={{ borderWidth: 1, borderColor: 'gray', margin: 10, padding: 5 }}
            onChangeText={setText}
            value={text}
          />

          <Button title="Choisir une image" onPress={pickImage} />

          {image && <Image source={{ uri: image }} style={{ width: 200, height: 200, margin: 10 }} />}

          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <TouchableOpacity onPress={onClose}>
              <Text>Annuler</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleAdd}>
              <Text>Ajouter</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default CreateRoutine;
