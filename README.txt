===========***PRESENTATION***===========

Cette application codée en React Native avec Expo. Expo (https://expo.dev/) permet de faciliter le test sur tablette et le build de l'application.

Les noms de classes, variables, méthodes et fonctions sont en anglais et en CamelCase. Les classes ont une majuscule au début, les variables, fonctions et méthodes n'ont pas de majuscule au début.

La base de données est une base de données locale SQLite.

==============***ACTIONS***=============

lancer l'appli sur une tablette:
    - installer expo go sur la tablette (peut se faire avec google play store)
    - connecter l'ordinateur et la tablette au même réseau wifi
    - dans la console au niveau du projet, lancer la commande : npm start
    - depuis l'appli expo go, scanner le QR code ou entrer  manuellement l'URL qui s'affichent dans la console  (dans certains IDE, le QR code n'est pas utilisable)

build de l'appli (nécessite un compte expo):
    la première fois:
        - lancer la commande : npm install --global expo-cli eas-cli
    à chaque fois (pour android):
        - eas build -p android --profile preview
        - se connecter à https://expo.dev/ pour télécharger l'apk
    si besoin de plus de détails:
        aller voir sur : https://www.lahoregraphix.com/creating-apk-for-android-in-react-native-expo-eas-build-updated-2023/

=============***ELEMENTS***=============

App.js : fichier à la base de l'application
Components : dossier contenant les classes Métier
db :  dossier contenant les classes java permettant l'accès à la base de données
Globals : dossier contenant le fichier java contenant les variables globales
Pages : dossier contenant les fichiers java des pages de l'application
assets : dossier pour stocker les ressources statiques (images, polices de caractères, fichiers audio, etc)

.gitignore : fichier de gestion de fichiers versionnés dans le Git
app.json : fichier de configuration pour les projets React Native avec Expo
eas.json : fichier de gestion des différents build avec expo

.expo : dossier contenant les fichiers de configuration du projet expo
node_module : dossier de gestion des dépendances des projets Node.js
babel.config.js : fichier de configuration de Babel, un outil de compilation JavaScript.
package-lock.json : fichier généré automatiquement
package.json : fichier de configuration du projet avec des infos sur les dépendances, les versions de packages, etc

